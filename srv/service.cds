using TEST from '../db/schema';

@path     : '/test'
service TestService {
    @odata.draft.enabled
    entity MAIN             as projection on TEST.MAIN;
    entity CONNECTED_ENTITY as projection on TEST.CONNECTED_ENTITY;
}
