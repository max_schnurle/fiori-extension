FROM gitpod/workspace-full
RUN sudo apt-get install ca-certificates && sudo apt update
RUN npm install -g npm@latest
# Install custom tools, runtime, etc.
RUN npm i -g @sap/cds-dk
# Install ui5/cli
RUN npm i -g @ui5/cli
# Install cf & multiapps-plugin
RUN curl -L "https://packages.cloudfoundry.org/stable?release=linux64-binary&version=v7&source=github" | tar -zx
RUN sudo mv cf7 /usr/local/bin
RUN sudo mv cf /usr/local/bin
RUN cf install-plugin multiapps -f
# install mbt
RUN curl -L "https://github.com/SAP/cloud-mta-build-tool/releases/download/v1.1.0/cloud-mta-build-tool_1.1.0_Linux_amd64.tar.gz" | tar -zx
RUN sudo mv mbt /usr/local/bin
# update npm
RUN npm install -g npm
RUN npm install hana-cli -g
