namespace TEST;

using {
    managed,
    cuid
} from '@sap/cds/common';


entity MAIN : cuid, managed {
    NAME             : String @Common.Label : 'Name Parent';
    CONNECTED_ENTITY : Composition of many CONNECTED_ENTITY
                           on CONNECTED_ENTITY.parent = $self;
    
};


entity CONNECTED_ENTITY : cuid, managed {
    NAME   : String @Common.Label : 'Name Child';
    parent : Association to one MAIN;
};
