sap.ui.define([], function () {
    "use strict";

    return {
        onPressCreate: function (event) {
            const list = this.byId("idEntityList");
            const binding = list.getBinding("items");
            this.getBindingContext().getModel().createBindingContext(this.getBindingContext().getPath()).getModel().bindList(binding.getPath(), this.getBindingContext()).create({ NAME: "Test Line Item" + Math.random() }).created().then(function () {
                binding.refreshInternal();
            });
        },
    };
});