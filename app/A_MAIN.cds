using TestService as service from '../srv/service';


annotate TestService.MAIN with {
    ID   @UI.Hidden  @Core.Computed;
    NAME @Common.Label : 'Name';

};


annotate TestService.MAIN with @(UI : {

    Facets              : [{
        $Type  : 'UI.ReferenceFacet',
        Target : '@UI.FieldGroup#Details',
        Label  : 'Data',
        ID: 'Data'
    }, ],
    SelectionFields     : [NAME,

    ],
    LineItem            : [{
        $Type : 'UI.DataField',
        Value : NAME,
    },

    ],

    HeaderInfo          : {
        $Type          : 'UI.HeaderInfoType',
        TypeName       : 'Test',
        TypeNamePlural : 'Tests',
        Title          : {Value : NAME}
    },
    FieldGroup #Details : {
        $Type : 'UI.FieldGroupType',
        Data  : [{
            $Type : 'UI.DataField',
            Value : NAME,
        },


        ],
    },
});
